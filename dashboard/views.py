from django.shortcuts import render, redirect
from .models import UserDataModel
from .forms import UserDatForm

# Create your views here.


def index(request):
    data = UserDataModel.objects.all()
    if request.method == 'POST':
        form = UserDatForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = UserDatForm()
    context = {
        'data': data,
        'form': form,
    }
    return render(request, 'index.html', context)
