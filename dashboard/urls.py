from django.urls import path
from . import views

"""
    these are api endpoints for incoming requests.
    if the request resourse matches the endpoint path,
    the endpoint will call a handler in view to handle that request
"""
urlpatterns = [
    path('', views.index, name='index'),
]
