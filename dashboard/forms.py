from django import forms
from .models import UserDataModel


class UserDatForm(forms.ModelForm):
    class Meta:
        model = UserDataModel
        file_field = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
        fields = '__all__'
