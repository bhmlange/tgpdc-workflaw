from django.db import models


# Create your models here.
class UserDataModel(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    date_of_birth = models.IntegerField()
        

    # def __str__(self):
    #     return f'{self.country}-{self.population}'
