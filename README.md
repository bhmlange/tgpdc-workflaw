## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Usage](#usage)
* [License](#license)

## General info
tgpdc-workflaw is an extendable web-based workflow platform that is driven by human input(via HTML form).
The work flaw is used to get data from excel into the system and it renders visualisations of that data.

## Technologies
Project is created with:
* Django : 4.0.1
* python3 : 3.8.10
* chartjs:  3.7.1
* HTML5

## Setup
Clone the git repository of this project by running
```bash
git clone git@gitlab.com:bhmlange/tgpdc-workflaw.git on your CLI
```
and follow the instructions bellow

## Usage
```python
# uses default port number 8000
python3 admin.py server

# uses a specified port number
python3 admin.py server <port number>
```


## License
[MIT](https://choosealicense.com/licenses/mit/)
